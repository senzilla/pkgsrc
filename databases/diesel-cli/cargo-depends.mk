# $NetBSD: cargo-depends.mk,v 1.1 2023/06/26 11:47:35 wiz Exp $

CARGO_CRATE_DEPENDS+=	aho-corasick-1.0.1
CARGO_CRATE_DEPENDS+=	android_system_properties-0.1.5
CARGO_CRATE_DEPENDS+=	anstream-0.3.2
CARGO_CRATE_DEPENDS+=	anstyle-1.0.0
CARGO_CRATE_DEPENDS+=	anstyle-parse-0.2.0
CARGO_CRATE_DEPENDS+=	anstyle-query-1.0.0
CARGO_CRATE_DEPENDS+=	anstyle-wincon-1.0.1
CARGO_CRATE_DEPENDS+=	argon2-0.5.0
CARGO_CRATE_DEPENDS+=	assert_matches-1.5.0
CARGO_CRATE_DEPENDS+=	autocfg-1.1.0
CARGO_CRATE_DEPENDS+=	base64ct-1.6.0
CARGO_CRATE_DEPENDS+=	bigdecimal-0.3.1
CARGO_CRATE_DEPENDS+=	bitflags-1.3.2
CARGO_CRATE_DEPENDS+=	bitflags-2.3.1
CARGO_CRATE_DEPENDS+=	blake2-0.10.6
CARGO_CRATE_DEPENDS+=	block-buffer-0.10.4
CARGO_CRATE_DEPENDS+=	bumpalo-3.13.0
CARGO_CRATE_DEPENDS+=	byteorder-1.4.3
CARGO_CRATE_DEPENDS+=	cc-1.0.79
CARGO_CRATE_DEPENDS+=	cfg-if-1.0.0
CARGO_CRATE_DEPENDS+=	chrono-0.4.24
CARGO_CRATE_DEPENDS+=	clap-4.3.0
CARGO_CRATE_DEPENDS+=	clap_builder-4.3.0
CARGO_CRATE_DEPENDS+=	clap_complete-4.3.0
CARGO_CRATE_DEPENDS+=	clap_derive-4.3.0
CARGO_CRATE_DEPENDS+=	clap_lex-0.5.0
CARGO_CRATE_DEPENDS+=	colorchoice-1.0.0
CARGO_CRATE_DEPENDS+=	console-0.15.7
CARGO_CRATE_DEPENDS+=	core-foundation-sys-0.8.4
CARGO_CRATE_DEPENDS+=	crypto-common-0.1.6
CARGO_CRATE_DEPENDS+=	diffy-0.3.0
CARGO_CRATE_DEPENDS+=	digest-0.10.7
CARGO_CRATE_DEPENDS+=	dotenvy-0.15.7
CARGO_CRATE_DEPENDS+=	encode_unicode-0.3.6
CARGO_CRATE_DEPENDS+=	env_logger-0.8.4
CARGO_CRATE_DEPENDS+=	errno-0.3.1
CARGO_CRATE_DEPENDS+=	errno-dragonfly-0.1.2
CARGO_CRATE_DEPENDS+=	fastrand-1.9.0
CARGO_CRATE_DEPENDS+=	form_urlencoded-1.1.0
CARGO_CRATE_DEPENDS+=	generic-array-0.14.7
CARGO_CRATE_DEPENDS+=	getrandom-0.2.9
CARGO_CRATE_DEPENDS+=	hashbrown-0.12.3
CARGO_CRATE_DEPENDS+=	heck-0.4.1
CARGO_CRATE_DEPENDS+=	hermit-abi-0.3.1
CARGO_CRATE_DEPENDS+=	iana-time-zone-0.1.56
CARGO_CRATE_DEPENDS+=	iana-time-zone-haiku-0.1.2
CARGO_CRATE_DEPENDS+=	idna-0.3.0
CARGO_CRATE_DEPENDS+=	indexmap-1.9.3
CARGO_CRATE_DEPENDS+=	insta-1.29.0
CARGO_CRATE_DEPENDS+=	instant-0.1.12
CARGO_CRATE_DEPENDS+=	io-lifetimes-1.0.11
CARGO_CRATE_DEPENDS+=	ipnet-2.7.2
CARGO_CRATE_DEPENDS+=	ipnetwork-0.20.0
CARGO_CRATE_DEPENDS+=	is-terminal-0.4.7
CARGO_CRATE_DEPENDS+=	itoa-1.0.6
CARGO_CRATE_DEPENDS+=	js-sys-0.3.63
CARGO_CRATE_DEPENDS+=	lazy_static-1.4.0
CARGO_CRATE_DEPENDS+=	libc-0.2.144
CARGO_CRATE_DEPENDS+=	libsqlite3-sys-0.26.0
CARGO_CRATE_DEPENDS+=	linked-hash-map-0.5.6
CARGO_CRATE_DEPENDS+=	linux-raw-sys-0.3.8
CARGO_CRATE_DEPENDS+=	lock_api-0.4.9
CARGO_CRATE_DEPENDS+=	log-0.4.17
CARGO_CRATE_DEPENDS+=	memchr-2.5.0
CARGO_CRATE_DEPENDS+=	mysqlclient-sys-0.2.5
CARGO_CRATE_DEPENDS+=	nu-ansi-term-0.46.0
CARGO_CRATE_DEPENDS+=	num-bigint-0.4.3
CARGO_CRATE_DEPENDS+=	num-integer-0.1.45
CARGO_CRATE_DEPENDS+=	num-traits-0.2.15
CARGO_CRATE_DEPENDS+=	once_cell-1.17.1
CARGO_CRATE_DEPENDS+=	overload-0.1.1
CARGO_CRATE_DEPENDS+=	parking_lot-0.12.1
CARGO_CRATE_DEPENDS+=	parking_lot_core-0.9.7
CARGO_CRATE_DEPENDS+=	password-hash-0.5.0
CARGO_CRATE_DEPENDS+=	percent-encoding-2.2.0
CARGO_CRATE_DEPENDS+=	pkg-config-0.3.27
CARGO_CRATE_DEPENDS+=	ppv-lite86-0.2.17
CARGO_CRATE_DEPENDS+=	pq-sys-0.4.8
CARGO_CRATE_DEPENDS+=	proc-macro2-1.0.59
CARGO_CRATE_DEPENDS+=	quickcheck-1.0.3
CARGO_CRATE_DEPENDS+=	quote-1.0.28
CARGO_CRATE_DEPENDS+=	r2d2-0.8.10
CARGO_CRATE_DEPENDS+=	rand-0.8.5
CARGO_CRATE_DEPENDS+=	rand_chacha-0.3.1
CARGO_CRATE_DEPENDS+=	rand_core-0.6.4
CARGO_CRATE_DEPENDS+=	redox_syscall-0.2.16
CARGO_CRATE_DEPENDS+=	redox_syscall-0.3.5
CARGO_CRATE_DEPENDS+=	regex-1.8.3
CARGO_CRATE_DEPENDS+=	regex-syntax-0.7.2
CARGO_CRATE_DEPENDS+=	rustix-0.37.19
CARGO_CRATE_DEPENDS+=	ryu-1.0.13
CARGO_CRATE_DEPENDS+=	scheduled-thread-pool-0.2.7
CARGO_CRATE_DEPENDS+=	scopeguard-1.1.0
CARGO_CRATE_DEPENDS+=	serde-1.0.163
CARGO_CRATE_DEPENDS+=	serde_derive-1.0.163
CARGO_CRATE_DEPENDS+=	serde_json-1.0.96
CARGO_CRATE_DEPENDS+=	serde_regex-1.1.0
CARGO_CRATE_DEPENDS+=	serde_spanned-0.6.2
CARGO_CRATE_DEPENDS+=	similar-2.2.1
CARGO_CRATE_DEPENDS+=	smallvec-1.10.0
CARGO_CRATE_DEPENDS+=	strsim-0.10.0
CARGO_CRATE_DEPENDS+=	subtle-2.5.0
CARGO_CRATE_DEPENDS+=	syn-2.0.17
CARGO_CRATE_DEPENDS+=	tempfile-3.5.0
CARGO_CRATE_DEPENDS+=	time-0.3.21
CARGO_CRATE_DEPENDS+=	time-core-0.1.1
CARGO_CRATE_DEPENDS+=	time-macros-0.2.9
CARGO_CRATE_DEPENDS+=	tinyvec-1.6.0
CARGO_CRATE_DEPENDS+=	tinyvec_macros-0.1.1
CARGO_CRATE_DEPENDS+=	toml-0.7.4
CARGO_CRATE_DEPENDS+=	toml_datetime-0.6.2
CARGO_CRATE_DEPENDS+=	toml_edit-0.19.10
CARGO_CRATE_DEPENDS+=	typenum-1.16.0
CARGO_CRATE_DEPENDS+=	unicode-bidi-0.3.13
CARGO_CRATE_DEPENDS+=	unicode-ident-1.0.9
CARGO_CRATE_DEPENDS+=	unicode-normalization-0.1.22
CARGO_CRATE_DEPENDS+=	url-2.3.1
CARGO_CRATE_DEPENDS+=	utf8parse-0.2.1
CARGO_CRATE_DEPENDS+=	uuid-1.3.3
CARGO_CRATE_DEPENDS+=	vcpkg-0.2.15
CARGO_CRATE_DEPENDS+=	version_check-0.9.4
CARGO_CRATE_DEPENDS+=	wasi-0.11.0+wasi-snapshot-preview1
CARGO_CRATE_DEPENDS+=	wasm-bindgen-0.2.86
CARGO_CRATE_DEPENDS+=	wasm-bindgen-backend-0.2.86
CARGO_CRATE_DEPENDS+=	wasm-bindgen-macro-0.2.86
CARGO_CRATE_DEPENDS+=	wasm-bindgen-macro-support-0.2.86
CARGO_CRATE_DEPENDS+=	wasm-bindgen-shared-0.2.86
CARGO_CRATE_DEPENDS+=	winapi-0.3.9
CARGO_CRATE_DEPENDS+=	winapi-i686-pc-windows-gnu-0.4.0
CARGO_CRATE_DEPENDS+=	winapi-x86_64-pc-windows-gnu-0.4.0
CARGO_CRATE_DEPENDS+=	windows-0.48.0
CARGO_CRATE_DEPENDS+=	windows-sys-0.45.0
CARGO_CRATE_DEPENDS+=	windows-sys-0.48.0
CARGO_CRATE_DEPENDS+=	windows-targets-0.42.2
CARGO_CRATE_DEPENDS+=	windows-targets-0.48.0
CARGO_CRATE_DEPENDS+=	windows_aarch64_gnullvm-0.42.2
CARGO_CRATE_DEPENDS+=	windows_aarch64_gnullvm-0.48.0
CARGO_CRATE_DEPENDS+=	windows_aarch64_msvc-0.42.2
CARGO_CRATE_DEPENDS+=	windows_aarch64_msvc-0.48.0
CARGO_CRATE_DEPENDS+=	windows_i686_gnu-0.42.2
CARGO_CRATE_DEPENDS+=	windows_i686_gnu-0.48.0
CARGO_CRATE_DEPENDS+=	windows_i686_msvc-0.42.2
CARGO_CRATE_DEPENDS+=	windows_i686_msvc-0.48.0
CARGO_CRATE_DEPENDS+=	windows_x86_64_gnu-0.42.2
CARGO_CRATE_DEPENDS+=	windows_x86_64_gnu-0.48.0
CARGO_CRATE_DEPENDS+=	windows_x86_64_gnullvm-0.42.2
CARGO_CRATE_DEPENDS+=	windows_x86_64_gnullvm-0.48.0
CARGO_CRATE_DEPENDS+=	windows_x86_64_msvc-0.42.2
CARGO_CRATE_DEPENDS+=	windows_x86_64_msvc-0.48.0
CARGO_CRATE_DEPENDS+=	winnow-0.4.6
CARGO_CRATE_DEPENDS+=	yaml-rust-0.4.5
