# $NetBSD: versions.mk,v 1.34 2023/06/27 08:33:10 adam Exp $
# Do not edit, regenerate using 'make versions'
_VERSIONS=	compiler 8.3.1 erts 14.0.1 inets 9.0.1 kernel 9.0.1 ssl 11.0.1 stdlib 5.0.1 xmerl 1.3.32 asn1 5.1 common_test 1.25 crypto 5.2 debugger 5.3.1 dialyzer 5.1 diameter 2.3 edoc 1.2 eldap 1.2.11 erl_docgen 1.5 erl_interface 5.4 et 1.7 eunit 2.8.2 ftp 1.2 jinterface 1.14 megaco 4.4.4 mnesia 4.22 observer 2.15 odbc 2.14.1 os_mon 2.9 parsetools 2.5 public_key 1.14 reltool 1.0 runtime_tools 2.0 sasl 4.2.1 snmp 5.14 ssh 5.0 syntax_tools 3.1 tftp 1.1 tools 3.6 wx 2.3
