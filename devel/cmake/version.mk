# $NetBSD: version.mk,v 1.42 2023/08/12 12:50:12 adam Exp $
# used by devel/cmake/Makefile.common
# used by devel/cmake-fedora/Makefile

CMAKE_VERSION=	3.27.2
CMAKE_API=	${CMAKE_VERSION:R}
