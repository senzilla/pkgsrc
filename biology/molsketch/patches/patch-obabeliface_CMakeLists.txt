$NetBSD: patch-obabeliface_CMakeLists.txt,v 1.1 2023/04/18 08:49:21 pin Exp $

Update C++ standard from 14 to 17 for CMake.
https://github.com/hvennekate/Molsketch/commit/2726563113a3db1af0c667ec496a8569fa8ea8cd

--- obabeliface/CMakeLists.txt.orig	2023-04-14 16:04:22.000000000 +0000
+++ obabeliface/CMakeLists.txt
@@ -17,7 +17,7 @@ include_directories(${OPENBABEL_INCLUDE_
 # Create the obabeliface library
 add_library(obabeliface_LIB SHARED ${obabeliface_SRCS})
 set_target_properties(obabeliface_LIB PROPERTIES OUTPUT_NAME "obabeliface")
-set_property(TARGET obabeliface_LIB PROPERTY CXX_STANDARD 14)
+set_property(TARGET obabeliface_LIB PROPERTY CXX_STANDARD 17)
 if(WIN32 OR WIN64)
         set_target_properties(obabeliface_LIB PROPERTIES PREFIX "")
 endif(WIN32 OR WIN64)
