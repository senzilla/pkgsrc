$NetBSD: patch-requirements_base.txt,v 1.1 2022/10/31 17:32:46 adam Exp $

contextvars module is included in Python 3.7+.

--- requirements/base.txt.orig	2022-10-31 15:04:12.000000000 +0000
+++ requirements/base.txt
@@ -5,5 +5,4 @@ PyYAML
 MarkupSafe
 requests>=1.0.0
 distro>=1.0.1
-contextvars
 psutil>=5.0.0
