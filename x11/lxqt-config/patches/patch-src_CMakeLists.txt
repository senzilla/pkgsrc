$NetBSD: patch-src_CMakeLists.txt,v 1.2 2022/11/12 05:09:25 gutteridge Exp $

Install config file to proper location

--- src/CMakeLists.txt.orig	2020-11-02 09:35:25.000000000 +0000
+++ src/CMakeLists.txt
@@ -67,6 +67,6 @@ install(FILES
     COMPONENT Runtime
 )
 install(FILES lxqt-config.menu
-    DESTINATION "${LXQT_ETC_XDG_DIR}/menus"
+    DESTINATION "share/examples/menus"
     COMPONENT Runtime
 )
